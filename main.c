
#include <stdio.h>
#include <string.h>
#include "mm.h"
#include "unity.h"

#define ARRAY_SIZE(x)   (sizeof(x) / sizeof(x[0]))

typedef struct mm_hdr
{
    uint32_t size;
    int32_t total;
    int32_t free;
} mm_hdr_t;

typedef struct mm_data
{
    uint32_t size;
    uint8_t pattern;
} mm_data_t;

typedef struct mm_blk
{
    uint8_t dummy;
} mm_blk_t;

typedef union mm_container
{
    mm_hdr_t h;
    mm_data_t d;
    mm_blk_t b;
} mm_container_t;

typedef enum mm_type
{
    mm_hdr = 0,
    mm_blk,
    mm_dat,
    mm_dat_init,
} mm_type_e;

typedef struct mm_layout
{
    mm_type_e t;
    mm_container_t c;
} mm_layout_t;

static mm_handle_t hnd;
uint32_t heap[512];

static void mm_check_sequence(mm_handle_t *h, uint32_t *sizes)
{
    mm_head_t *head = h->list;
    int32_t index = 0;

    while (head)
    {
        TEST_ASSERT(head->block_size == sizes[index++]);
        head = head->next;
    }
}

static void mm_check_layout(void *heap, mm_layout_t *layout, uint32_t layout_len)
{
    uint8_t *ptr = heap;

    for (int32_t i=0; i < layout_len; ++i)
    {
        switch (layout[i].t)
        {
        case mm_hdr:
            {
                mm_head_t *head = (mm_head_t *)ptr;
                TEST_ASSERT(head->block_size == layout[i].c.h.size);
                TEST_ASSERT(head->free_blocks == layout[i].c.h.free);
                TEST_ASSERT(head->total_blocks == layout[i].c.h.total);

                ptr += sizeof(mm_head_t);
            }
            break;

        case mm_blk:
            {
                mm_block_t *block = (mm_block_t *)ptr;
                TEST_ASSERT(MM_WATERMARK == block->watermark);

                ptr += sizeof(mm_block_t);
            }
            break;

        case mm_dat:
            {
                uint32_t size = layout[i].c.d.size;
                for (uint32_t j=0; j < size; ++j)
                {
                    if (ptr[j] != layout[i].c.d.pattern)
                    {
                        printf("miss %d (%x)\n", i, ptr[j]);
                    }
                    TEST_ASSERT(ptr[j] == layout[i].c.d.pattern)
                }

                TEST_ASSERT(MM_WATERMARK == *((uint32_t *)(ptr + size)));

                ptr += (size + sizeof(uint32_t));
            }
            break;

        case mm_dat_init:
            {
                uint32_t size = layout[i].c.d.size;
                for (uint32_t j=0; j < size; ++j)
                {
                    if (ptr[j] != layout[i].c.d.pattern)
                    {
                        printf("miss %d (%x)\n", i, ptr[j]);
                    }
                    TEST_ASSERT(ptr[j] == layout[i].c.d.pattern)
                }

                ptr += size;
            }
            break;
        }
    }
}

#if 1
static void mm_dump_heap(uint32_t *heap, uint32_t len)
{
    int32_t i;

    for (i=0; i < len; ++i)
    {
        printf("%08X: %08X\n", i, heap[i]);
    }
}
#endif

void setUp(void)
{
    memset(heap, 0xFF, sizeof(heap));
}

void tearDown(void)
{
}

void test_init_too_large(void)
{
    int32_t rv;
    mm_segment_t seg1[] = 
    {
        {1024, 4},
        {0, 0}
    };

    rv = mm_init(&hnd, heap, sizeof(heap), seg1);
    TEST_ASSERT_EQUAL(-1, rv);

    mm_segment_t seg2[] = 
    {
        {8, 1},
        {0, 0}
    };

    rv = mm_init(&hnd, heap, 26, seg2);
    TEST_ASSERT_EQUAL(-1, rv);
}

void test_init_basic(void)
{
    mm_segment_t seg[] = 
    {
        {16, 1},
        {30, 2},
        {64, 3},
        {0, 0}
    };

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {16, 1, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},

        {.t=mm_hdr, .c.h = {32, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0xff}},

        {.t=mm_hdr, .c.h = {64, 3, 3}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {64, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {64, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {64, 0xff}}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_malloc_invalid_args(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {16, 2},
        {0, 0}
    };

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {16, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };

    int32_t rv = mm_init(&hnd, heap, 92, seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 0);
    TEST_ASSERT_NULL(ptr);

    ptr = mm_malloc(&hnd, 32);
    TEST_ASSERT_NULL(ptr);

    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_malloc_same(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, 90, seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x11, 16);

    mm_layout_t layout1[] =
    {
        {.t=mm_hdr, .c.h = {16, 2, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}}
    };
    mm_check_layout(heap, layout1, ARRAY_SIZE(layout1));

    ptr = mm_malloc(&hnd, 15);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x22, 16);

    mm_layout_t layout2[] =
    {
        {.t=mm_hdr, .c.h = {16, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x22}}
    };
    mm_check_layout(heap, layout2, ARRAY_SIZE(layout2));

    ptr = mm_malloc(&hnd, 14);
    TEST_ASSERT_NULL(ptr);

    mm_check_layout(heap, layout2, ARRAY_SIZE(layout2));
}

void test_malloc_next_larger(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {16, 1},
        {32, 1},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x11, 16);

    mm_layout_t layout1[] =
    {
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_hdr, .c.h = {32, 1, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0xff}},
    };
    mm_check_layout(heap, layout1, ARRAY_SIZE(layout1));

    ptr = mm_malloc(&hnd, 15);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x22, 32);

    mm_layout_t layout2[] =
    {
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_hdr, .c.h = {32, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0x22}},
    };
    mm_check_layout(heap, layout2, ARRAY_SIZE(layout2));
}

void test_malloc_too_large(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 32768);
    TEST_ASSERT_NULL(ptr);

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {16, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}}
    
    };

    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_malloc_new_block(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {8, 1},
        {16, 1},
        {32, 1},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 8);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x11, 8);

    ptr = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x22, 16);

    ptr = mm_malloc(&hnd, 32);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x33, 32);

    mm_layout_t layout1[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x11}},
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x22}},
        {.t=mm_hdr, .c.h = {32, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0x33}},
    };
    mm_check_layout(heap, layout1, ARRAY_SIZE(layout1));

    ptr = mm_malloc(&hnd, 6);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x44, 8);

    mm_layout_t layout2[] =
    {
        {.t=mm_hdr, .c.h = {8, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x11}},
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x22}},
        {.t=mm_hdr, .c.h = {32, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0x33}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x44}},
    };
    mm_check_layout(heap, layout2, ARRAY_SIZE(layout2));

    ptr = mm_malloc(&hnd, 14);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x55, 16);

    mm_layout_t layout3[] =
    {
        {.t=mm_hdr, .c.h = {8, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x11}},
        {.t=mm_hdr, .c.h = {16, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x22}},
        {.t=mm_hdr, .c.h = {32, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0x33}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x44}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x55}},
    };
    mm_check_layout(heap, layout3, ARRAY_SIZE(layout3));

    ptr = mm_malloc(&hnd, 30);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x66, 32);

    mm_layout_t layout4[] =
    {
        {.t=mm_hdr, .c.h = {8, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x11}},
        {.t=mm_hdr, .c.h = {16, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x22}},
        {.t=mm_hdr, .c.h = {32, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0x33}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x44}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x55}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0x66}},
    };
    mm_check_layout(heap, layout4, ARRAY_SIZE(layout4));
}

void test_malloc_new_segment(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {16, 1},
        {64, 1},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x11, 16);

    ptr = mm_malloc(&hnd, 64);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x22, 64);

    mm_layout_t layout1[] =
    {
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_hdr, .c.h = {64, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {64, 0x22}},
    };
    mm_check_layout(heap, layout1, ARRAY_SIZE(layout1));

    ptr = mm_malloc(&hnd, 6);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x33, 8);

    mm_layout_t layout2[] =
    {
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_hdr, .c.h = {64, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {64, 0x22}},
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x33}},
    };
    mm_check_layout(heap, layout2, ARRAY_SIZE(layout2));

    ptr = mm_malloc(&hnd, 128);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x44, 128);

    mm_layout_t layout3[] =
    {
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_hdr, .c.h = {64, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {64, 0x22}},
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x33}},
        {.t=mm_hdr, .c.h = {128, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {128, 0x44}},
    };
    mm_check_layout(heap, layout3, ARRAY_SIZE(layout3));

    ptr = mm_malloc(&hnd, 32);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x55, 32);

    mm_layout_t layout4[] =
    {
        {.t=mm_hdr, .c.h = {16, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x11}},
        {.t=mm_hdr, .c.h = {64, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {64, 0x22}},
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x33}},
        {.t=mm_hdr, .c.h = {128, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {128, 0x44}},
        {.t=mm_hdr, .c.h = {32, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {32, 0x55}},
    };
    mm_check_layout(heap, layout4, ARRAY_SIZE(layout4));

    uint32_t sizes[] = {8, 16, 32, 64, 128};
    mm_check_sequence(&hnd, sizes);
}

void test_malloc_empty(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 8);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x11, 8);

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x11}}
    };
    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_free_invalid_args(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 12);
    TEST_ASSERT_NOT_NULL(ptr);

    // Free a non-existing block and check statistics
    mm_free(&hnd, NULL);

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {16, 2, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };

    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_free_basic(void)
{
    uint8_t *ptr[4] = { NULL };
    int32_t rv;
    mm_segment_t seg[] = 
    {
        {8, 2},
        {16, 2},
        {0, 0}
    };

    rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    // Allocate all blocks and fill them with a pattern
    ptr[0] = mm_malloc(&hnd, 8);
    TEST_ASSERT_NOT_NULL(ptr[0]);
    memset(ptr[0], 0x11, 8);

    ptr[1] = mm_malloc(&hnd, 8);
    TEST_ASSERT_NOT_NULL(ptr[1]);
    memset(ptr[1], 0x22, 8);

    ptr[2] = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr[2]);
    memset(ptr[2], 0x33, 16);

    ptr[3] = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr[3]);
    memset(ptr[3], 0x44, 16);

    // Free the first block (size 16), allocate again & change the pattern
    mm_free(&hnd, ptr[2]);
    ptr[2] = NULL;
    ptr[2] = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr[2]);
    memset(ptr[2], 0x55, 16);

    // Free the second block (size 16), allocate again & change the pattern
    mm_free(&hnd, ptr[3]);
    ptr[3] = NULL;
    ptr[3] = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr[3]);
    memset(ptr[3], 0x66, 16);

    // Free the second block (size 8), allocate again & change the pattern
    mm_free(&hnd, ptr[1]);
    ptr[1] = NULL;
    ptr[1] = mm_malloc(&hnd, 8);
    TEST_ASSERT_NOT_NULL(ptr[1]);
    memset(ptr[1], 0x77, 8);

    // Free the first block (size 8), allocate again & change the pattern
    mm_free(&hnd, ptr[0]);
    ptr[0] = NULL;
    ptr[0] = mm_malloc(&hnd, 8);
    TEST_ASSERT_NOT_NULL(ptr[0]);
    memset(ptr[0], 0x88, 8);

    // Check if the corresponding patterns changed
    mm_layout_t layout1[] =
    {
        {.t=mm_hdr, .c.h = {8, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x88}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x77}},

        {.t=mm_hdr, .c.h = {16, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x55}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x66}},
    };
    mm_check_layout(heap, layout1, ARRAY_SIZE(layout1));

    // Free the first block (size 8) and check statistics
    mm_free(&hnd, ptr[0]);

    mm_layout_t layout2[] =
    {
        {.t=mm_hdr, .c.h = {8, 2, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x88}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x77}},

        {.t=mm_hdr, .c.h = {16, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x55}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x66}},
    };
    mm_check_layout(heap, layout2, ARRAY_SIZE(layout2));

    // Free the second block (size 8) and check statistics
    mm_free(&hnd, ptr[1]);

    mm_layout_t layout3[] =
    {
        {.t=mm_hdr, .c.h = {8, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x88}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x77}},

        {.t=mm_hdr, .c.h = {16, 2, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x55}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0x66}},
    };
    mm_check_layout(heap, layout3, ARRAY_SIZE(layout3));
}

void test_realloc_basic(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {8, 1},
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    // Reallocate without initial memory (allocation)
    ptr = mm_realloc(&hnd, NULL, 7);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x11, 8);

    mm_layout_t layout1[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x11}},

        {.t=mm_hdr, .c.h = {16, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout1, ARRAY_SIZE(layout1));

    // Reallocate to smaller size (only pointer copy)
    ptr = mm_realloc(&hnd, ptr, 4);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x22, 8);

    mm_layout_t layout2[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x22}},

        {.t=mm_hdr, .c.h = {16, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout2, ARRAY_SIZE(layout2));

    // Reallocate to the same size (only pointer copy)
    ptr = mm_realloc(&hnd, ptr, 4);
    TEST_ASSERT_NOT_NULL(ptr);
    memset(ptr, 0x33, 8);

    mm_layout_t layout3[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x33}},

        {.t=mm_hdr, .c.h = {16, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout3, ARRAY_SIZE(layout3));

    // Reallocate to a larger size (new allocation + copy + free existing)
    ptr = mm_realloc(&hnd, ptr, 12);
    TEST_ASSERT_NOT_NULL(ptr);

    mm_layout_t layout4[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x33}},
        {.t=mm_hdr, .c.h = {16, 2, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat_init, .c.d = {8, 0x33}},
        {.t=mm_dat, .c.d = {8, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout4, ARRAY_SIZE(layout4));

    // Reallocate to zero size (free)
    ptr = mm_realloc(&hnd, ptr, 0);
    TEST_ASSERT_NULL(ptr);

    mm_layout_t layout5[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0x33}},
        {.t=mm_hdr, .c.h = {16, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat_init, .c.d = {8, 0x33}},
        {.t=mm_dat, .c.d = {8, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout5, ARRAY_SIZE(layout5));
}

void test_realloc_too_large(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {8, 1},
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_malloc(&hnd, 16);
    TEST_ASSERT_NOT_NULL(ptr);

    ptr = mm_realloc(&hnd, ptr, 32768);
    TEST_ASSERT_NULL(ptr);

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0xff}},
        {.t=mm_hdr, .c.h = {16, 2, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_calloc_invalid_args(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {8, 1},
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_calloc(&hnd, 0, 1);
    TEST_ASSERT_NULL(ptr);

    ptr = mm_calloc(&hnd, 1, 0);
    TEST_ASSERT_NULL(ptr);

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0xff}},
        {.t=mm_hdr, .c.h = {16, 2, 2}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_calloc_basic(void)
{
    uint8_t *ptr = NULL;
    mm_segment_t seg[] = 
    {
        {8, 1},
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    ptr = mm_calloc(&hnd, 16, 1);
    TEST_ASSERT_NOT_NULL(ptr);

    mm_layout_t layout[] =
    {
        {.t=mm_hdr, .c.h = {8, 1, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {8, 0xff}},
        {.t=mm_hdr, .c.h = {16, 2, 1}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0}},
        {.t=mm_blk, .c.b = {0}},
        {.t=mm_dat, .c.d = {16, 0xff}},
    };
    mm_check_layout(heap, layout, ARRAY_SIZE(layout));
}

void test_print_basic(void)
{
    mm_segment_t seg[] = 
    {
        {8, 1},
        {16, 2},
        {0, 0}
    };

    int32_t rv = mm_init(&hnd, heap, sizeof(heap), seg);
    TEST_ASSERT_EQUAL(0, rv);

    mm_print(&hnd);
}

int main(int argc, char *argv[])
{
    UNITY_BEGIN();

    RUN_TEST(test_init_basic);
    RUN_TEST(test_init_too_large);

    RUN_TEST(test_malloc_invalid_args);
    RUN_TEST(test_malloc_same);
    RUN_TEST(test_malloc_next_larger);
    RUN_TEST(test_malloc_too_large);
    RUN_TEST(test_malloc_new_block);
    RUN_TEST(test_malloc_new_segment);
    RUN_TEST(test_malloc_empty);

    RUN_TEST(test_free_invalid_args);
    RUN_TEST(test_free_basic);

    RUN_TEST(test_realloc_basic);
    RUN_TEST(test_realloc_too_large);

    RUN_TEST(test_calloc_invalid_args);
    RUN_TEST(test_calloc_basic);

    RUN_TEST(test_print_basic);

    return UNITY_END();
}