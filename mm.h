
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Memory manager
 *
 **********************************************************************/

 /**
  * @addtogroup MM
  * @{
  */

#ifndef _MM_H_
#define _MM_H_

#include <stdint.h>
#include "mm_conf.h"

/// Performs 32-bit boundary aligment
#define MM_ALIGN(x) (((x) + sizeof(uint32_t) - 1) & ~(sizeof(uint32_t) - 1))

/// Memory manager free block list
typedef struct mm_block
{
#ifdef MM_USE_SAFE_CHECK
    uint32_t watermark;     /**< Start of block watermark */
#endif
    struct mm_head *head;   /**< Pointer to header */
    struct mm_block *next;  /**< Pointer to next free block */
    uint8_t data[0];        /**< Pointer to user data */
} mm_block_t;

/// Memory manager segment header list
typedef struct mm_head
{
    uint32_t block_size;        /**< Block size in bytes */
    mm_block_t *list;           /**< Free block list */
    struct mm_head *next;       /**< Pointer to next header */
#ifdef MM_USE_STATS
    int32_t total_blocks;       /**< Total number of free blocks */
    int32_t free_blocks;        /**< Current number of free blocks */
    int32_t min_free_blocks;    /**< Minimal number of free blocks */
#endif
} mm_head_t;

/// Memory manager segment structure
typedef struct mm_segment
{
    uint32_t block_size;    /**< Block size in bytes */
    int32_t total_blocks;   /**< Total number of free blocks */
} mm_segment_t;

/// Memory manager hande
typedef struct mm_handle
{
    uint32_t capacity;  /**< Heap capacity in bytes */
    mm_head_t *list;    /**< Header list */
    void *start;        /**< Beginning of heap */
    void *end;          /**< End of used heap */
} mm_handle_t;

int32_t mm_init(mm_handle_t *hnd, uint32_t *heap, uint32_t heap_size, mm_segment_t *seg);
void *mm_malloc(mm_handle_t *hnd, uint32_t size);
void *mm_realloc(mm_handle_t *hnd, void *ptr, uint32_t size);
void mm_free(mm_handle_t *hnd, void *ptr);
void *mm_calloc(mm_handle_t *hnd, uint32_t nitems, uint32_t size);
void mm_print(mm_handle_t *hnd);

#endif

/**
 * @}
 */
