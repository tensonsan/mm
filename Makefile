
TARGET = mm_test
CC = gcc
LDFLAGS = -lm
CFLAGS = -Wall -m32 -g -DUSE_TESTS=1 -fprofile-arcs -ftest-coverage
BUILDDIR = bin

SRC = $(wildcard *.c)
OBJ = $(addprefix $(BUILDDIR)/, $(addsuffix .o, $(basename $(SRC))))

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

$(BUILDDIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
	rm -f $(TARGET)
	rm -rf doc
	rm -rf cov
	rm -f *.info

run:
	./$(TARGET)

coverage:
	lcov --capture --directory . --output-file coverage.info
	genhtml coverage.info --output-directory cov

doc:
	doxygen Doxyfile
