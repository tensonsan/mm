## Simple memory allocator for embedded systems

### Features:

* Segregated explicit free list allocator (no block coalescence)
* 16 byte overhead per block and 24 byte overhead per size class (segment).
* Portable.
* C99 compliant.
* Unit tested.

### Porting:

* Copy [mm.c](mm.c), [mm.h](mm.h) and [mm_config](mm_conf.h) to your project.
* Edit the [mm_conf.h](mm_conf.h) according to your needs.
* Provide MM_ASSERT and MM_PRINT_DELAY macros in [mm_conf.h](mm_conf.h).

### Usage:

Generate Doxygen documentation for the library.
```Makefile
make doc
```

Compile the library into an executable, used in unit tests.
```Makefile
make
```

Run the unit tests ([Unity framework](http://www.throwtheswitch.org/unity) is used).
```Makefile
make run
```

Generate the unit test coverage report.
```Makefile
make coverage
```

Cleanup
```Makefile
make clean
```

### License:

Copyright 2019 Gasper Korinsek

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


