
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Memory manager configuration
 *
 **********************************************************************/

 /**
  * @addtogroup MM
  * @{
  */

#ifndef _MM_CONF_H_
#define _MM_CONF_H_

#include <stdio.h>
#include <assert.h>
#ifdef USE_TESTS
#include "unity.h"
#endif

/**
 * Uncomment to prevent allocation of avaiailable blocks that exceed the
 * required size. This can be used to determine what is the system's
 * actual memory use to tune the initial segmentation.
 * @note Using this define will increase the allocation time.
 */
//#define MM_ALLOC_EXACT_SIZE 1

/**
 * Uncomment to allow creation of new blocks during the allocation if the initial
 * segmentation wasn't optimal. This can be used to determine what is the system's
 * actual memory use to tune the initial segmentation.
 * @note Using this define will increase the allocation time.
 */
#define MM_ALLOW_NEW_ALLOC 1

/**
 * Uncomment to allow checking of watermarks at the beginning and the end of block
 * when freeing the memory. This can be used to determine if a memory overrun
 * happened around the allocated block.
 * @note Enabling this feature consumes 8 extra bytess per each block.
 */
#define MM_USE_SAFE_CHECK 1

/**
 * Uncomment for a more detailed memory management statistics. The \p mm_print
 * function will also show statistics per individual memory segment.
 * @note Enabling this feature consumes 12 extra bytes per each block list header.
 */
#define MM_USE_STATS 1

/// Set the printf function for statistics printout
#define MM_PRINTF(f, ...)   printf((f), ##__VA_ARGS__)

/// Set the assert function for debugging
#ifdef USE_TESTS
#define MM_ASSERT(expr) TEST_ASSERT(expr)
#else
#define MM_ASSERT(expr) assert(expr)
#endif

/// Set the delay for printouts to avoid memory overflow
#define MM_PRINT_DELAY()

#ifdef MM_USE_SAFE_CHECK
/// Watermark for the memory overrun block checking
#define MM_WATERMARK  0xdeadbeef
#endif

#endif

/**
 * @}
 */
