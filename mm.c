
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Memory manager
 *
 **********************************************************************/

 /**
  * @addtogroup MM
  * @{
  */

#include <string.h>
#include "mm.h"

/// Calculates used heap size
#define MM_USED_HEAP(hnd)   ((uint8_t *)((hnd)->end) - (uint8_t *)((hnd)->start))

/// Gets a pointer to the block header
#define MM_BLOCK_PTR(ptr)   ((mm_block_t *)(ptr) - 1)

/// Gets the end-watermark value
#define MM_END_WATERMARK(block) (*((uint32_t *)(&(block->data[block->head->block_size]))))

/**
 * @brief Gets a block of from the heap and advances to the next free location in the heap
 *
 * @param[in] hnd Memory manager handle
 * @param[in] size Size of a block in bytes
 *
 * @return Pointer to the block if successful or a NULL pointer
 */
static void *mm_get(mm_handle_t *hnd, uint32_t size)
{
    MM_ASSERT(hnd);

    void *p = NULL;

    if (MM_USED_HEAP(hnd) + size < hnd->capacity)
    {
        p = hnd->end;
        hnd->end = (uint8_t *)(hnd->end) + size;
    }

    return p;
}

/**
 * @brief Creates an new memory block and links it into an appropriate segment
 *
 * @param[in] hnd Memory manager handle
 * @param[in] head Segment header
 *
 * @return Pointer to the block if successful or a NULL pointer
 */
static mm_block_t *mm_block_create(mm_handle_t *hnd, mm_head_t *head)
{
    MM_ASSERT(hnd);
    MM_ASSERT(head);

    uint32_t sz = sizeof(mm_block_t) + head->block_size;
#ifdef MM_USE_SAFE_CHECK
    sz += sizeof(uint32_t);
#endif

    mm_block_t *block = mm_get(hnd, sz);
    if (NULL == block)
    {
        return block;
    }

    block->head = head;
    block->next = (mm_block_t *)hnd->end;
#ifdef MM_USE_SAFE_CHECK
    block->watermark = MM_WATERMARK;
    MM_END_WATERMARK(block) = MM_WATERMARK;
#endif

#ifdef MM_USE_STATS
    head->total_blocks++;
    head->free_blocks++;
    head->min_free_blocks++;
#endif

    return block;
}

/**
 * @brief Links an existing memory block into an appropriate segment
 *
 * @param[in] hnd Memory manager handle
 * @param[in] block Memory block
 */
static void mm_block_push(mm_handle_t *hnd, mm_block_t *block)
{
    MM_ASSERT(hnd);
    MM_ASSERT(block);

#ifdef MM_USE_STATS
    block->head->free_blocks++;
#endif

#ifdef MM_USE_SAFE_CHECK
    MM_ASSERT(MM_WATERMARK == block->watermark);
    MM_ASSERT(MM_WATERMARK == MM_END_WATERMARK(block));
#endif

    block->next = block->head->list;
    block->head->list = block;
}

/**
 * @brief Fetches a memory block from a memory segment
 *
 * @param[in] hnd Memory manager handle
 * @param[in] head Segment header
 *
 * @return Pointer to the block if successful or a NULL pointer
 */
static mm_block_t *mm_block_pop(mm_handle_t *hnd, mm_head_t *head)
{
    MM_ASSERT(hnd);
    MM_ASSERT(head);

    mm_block_t *block = head->list;

    MM_ASSERT(block);

#ifdef MM_USE_STATS
    head->free_blocks--;
    if (head->free_blocks < head->min_free_blocks)
    {
        head->min_free_blocks = head->free_blocks;
    }
#endif

    head->list = head->list->next;

    return block;
}

/**
 * @brief Creates an initial segmentation of the heap as specified in the \p seg array
 *
 * @warning Must be called once before any other function from this module!
 * @note The seg list must contain monotonically inreasing block sizes.
 *
 * @param[in] hnd Memory manager handle
 * @param[in] heap Pointer to heap buffer
 * @param[in] heap_size Heap size in bytes
 * @param[in] seg Memory segmentation array
 *
 * @retval -1 The heap size is insufficient to create a free block list
 * @retval 0 Initial heap segmentation created
 */
int32_t mm_init(mm_handle_t *hnd, uint32_t *heap, uint32_t heap_size, mm_segment_t *seg)
{
    MM_ASSERT(hnd);
    MM_ASSERT(heap);
    MM_ASSERT(seg);
    MM_ASSERT(heap_size);
    MM_ASSERT(heap_size > sizeof(mm_head_t));

    hnd->list = (mm_head_t *)heap;
    hnd->end = heap;
    hnd->start = heap;
    hnd->capacity = heap_size;

    // Clear the initial header to allow allocations without an initial segmentation
    mm_head_t *head = hnd->list;
    memset(head, 0, sizeof(mm_head_t));

    // Create an initial memory layout according to the seg
    while (seg->block_size)
    {
        // Create a new segment header
        head = mm_get(hnd, sizeof(mm_head_t));
        if (NULL == head)
        {
            return -1;
        }

        memset(head, 0, sizeof(mm_head_t));
        head->block_size = MM_ALIGN(seg->block_size);
        head->list = (mm_block_t *)hnd->end;

        // Create and add free blocks to the segment
        mm_block_t *block = NULL;
        for (uint32_t i=0; i < seg->total_blocks; ++i)
        {
            block = mm_block_create(hnd, head);
            if (NULL == block)
            {
                return -1;
            }
        }
        block->next = NULL;

        head->next = (mm_head_t *)hnd->end;

        seg++;
    }
    head->next = NULL;

    return 0;
}

/**
 * @brief Allocates a single block from the heap
 *
 * @note The function first looks for available free blocks in the existing heap segments.
 *       If there are none and the \p MM_ALLOW_NEW_ALLOC is defined, it will try to create
 *       a new free block or a segment list with a single block after the last segment.
 *
 * @param[in] hnd Memory manager handle
 * @param[in] size Size of block in bytes
 * 
 * @return Pointer to the allocated block if allocation successful or a NULL pointer
 */
void *mm_malloc(mm_handle_t *hnd, uint32_t size)
{
    MM_ASSERT(hnd);

    if (0 == size)
    {
        return NULL;
    }

    size = MM_ALIGN(size);
    mm_block_t *block = NULL;

    // Do we have a suitable existing block?
    mm_head_t *head = hnd->list;
    while(head)
    {
#ifdef MM_ALLOC_EXACT_SIZE
        if ((head->block_size == size) && head->list)
#else
        if ((head->block_size >= size) && head->list)
#endif
        {
            // Give a block to the user
            block = mm_block_pop(hnd, head);
            if (NULL == block)
            {
                return NULL;
            }
            return block->data;
        }
        head = head->next;
    }

#ifndef MM_ALLOW_NEW_ALLOC 
    return NULL;
#else
    // Try to add a block to the existing segment
    head = hnd->list;
    while (head)
    {
        if (head->block_size == size)
        {
            // Create a new block
            block = mm_block_create(hnd, head);
            if (NULL == block)
            {
                return NULL;
            }
            block->next = NULL;
            head->list = block;

            // Give a block to the user
            block = mm_block_pop(hnd, head);

            return block->data;
        }
        head = head->next;
    }

    // Create a new segment header
    mm_head_t *head_new = (mm_head_t *)mm_get(hnd, sizeof(mm_head_t));
    if (NULL == head_new)
    {
        return NULL;
    }

    memset(head_new, 0, sizeof(mm_head_t));
    head_new->block_size = size;

    // Create a new block
    block = mm_block_create(hnd, head_new);
    if (NULL == block)
    {
        return NULL;
    }
    block->next = NULL;
    head_new->list = block;

    // Insertion sort for new segment
    head = hnd->list;
    if (size < head->block_size)
    {
        head_new->next = head;
        hnd->list = head_new;
    }
    else
    {
        while (head->next)
        {
            if (size < head->next->block_size)
            {
                break;
            }
            head = head->next;
        }
        head_new->next = head->next;
        head->next = head_new;
    }

    // Give a block to the user
    block = mm_block_pop(hnd, head_new);

    return block->data;
#endif
}

/**
 * @brief Re-allocates a memory block
 *
 * @note If \p ptr is NULL, a new block of requested \p size is allocated.
 *       If \p size is 0, the existing memory block is freed.
 *       If \p size doesn't exceed the existing block size, the existing block is returned.
 *       If \p size exceeds the existing block size, a larger block is allocated and data is copied.
 *
 * @param[in] hnd Memory manager handle
 * @param[in] ptr Existing memory block
 * @param[in] size Size of the new block in bytes
 * 
 * @return Pointer to the reallocated block if successful or a NULL pointer
 */
void *mm_realloc(mm_handle_t *hnd, void *ptr, uint32_t size)
{
    uint8_t *ptr_out = NULL;

    if (NULL == ptr)
    {
        // Allocate a new block
        ptr_out = mm_malloc(hnd, size);
    }
    else if (0 == size)
    {
        // Free the old block if the requested memory size is 0
        mm_free(hnd, ptr);
    }
    else
    {
        size = MM_ALIGN(size);
        uint32_t size_old = MM_BLOCK_PTR(ptr)->head->block_size;

        if (size <= size_old)
        {
            // Return the old block if the requested memory size is smaller or equal
            ptr_out = ptr;
        }
        else
        {
            // Migrate data from a smaller block to the larger block
            ptr_out = mm_malloc(hnd, size);
            if (ptr_out)
            {
                memcpy(ptr_out, ptr, size_old);

                mm_free(hnd, ptr);
            }
        }
    }
    return (void *)ptr_out;
}

/**
 * @brief Returns a single block to the heap
 *
 * @param[in] hnd Memory manager handle
 * @param[in] ptr Pointer to block
 */
void mm_free(mm_handle_t *hnd, void *ptr)
{
    if (NULL != ptr)
    {
        mm_block_push(hnd, MM_BLOCK_PTR(ptr));
    }
}

/**
 * @brief Allocates a memory block and initializes it with zeros
 *
 * @param[in] hnd Memory manager handle
 * @param[in] nitems Number of items in a memory block
 * @param[in] size Size of an item in bytes
 * 
 * @return Pointer to the allocated block if successful or a NULL pointer
 */
void *mm_calloc(mm_handle_t *hnd, uint32_t nitems, uint32_t size)
{
    uint32_t sz = nitems * size;

    uint8_t *ptr = mm_malloc(hnd, sz);
    if (ptr)
    {
        memset(ptr, 0, sz);
    }

    return (void *)ptr;
}

/**
 * @brief Prints the memory manager statistics
 *
 * @param[in] hnd Memory manager handle
 */
void mm_print(mm_handle_t *hnd)
{
    MM_ASSERT(hnd);

    // Print the heap usage
    MM_PRINTF("\n\rheap used:     %d bytes\n\r", MM_USED_HEAP(hnd));
    MM_PRINTF("heap capacity: %d bytes\n\r", hnd->capacity);

    // Traverse the header list and print statistics
    mm_head_t *head = hnd->list;
    while (head)
    {
        MM_PRINTF(" block size:      %d bytes\n\r", head->block_size);
#ifdef MM_USE_STATS
        MM_PRINTF(" total blocks:    %d\n\r", head->total_blocks);
        MM_PRINTF(" free blocks:     %d\n\r", head->free_blocks);
        MM_PRINTF(" min free blocks: %d\n\r", head->min_free_blocks);
#endif
        MM_PRINT_DELAY();
        head = head->next;
    }
}

/**
 * @}
 */
